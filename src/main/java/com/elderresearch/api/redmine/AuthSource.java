/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import lombok.Data;
import lombok.experimental.Accessors;

//NOTE: Requires customizations to Redmine 3.4 to expose auth sources via API
@Data
@Accessors(chain = true)
public class AuthSource {
	private Integer id;
	private String type;
	private String name;
	private String host;
	private Integer port;
	private String account;
	private String accountPassword;
	private String baseDn;
	private String attrLogin;
	private String attrFirstname;
	private String attrLastname;
	private String attrMail;
	private Boolean ontheflyRegister;
	private Boolean tls;
	private String filter;
	private Integer timeout;
}
