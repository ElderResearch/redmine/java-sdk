/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CustomField extends Identifiable {
	private String customizedType;
	private String fieldFormat;
	private String description;
	private String regexp;
	private Integer minLength;
	private Integer maxLength;
	private boolean required;
	private boolean filter;
	private boolean searchable;
	private boolean multiple;
	private boolean visible;
	private String defaultValue;
}
