/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.io.Serializable;
import java.util.Collection;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomFieldValue implements Serializable {
	private int id;
	private String name;
	private Object value;
	private Boolean multiple;
	
	public CustomFieldValue setValue(Object value) {
		this.value = value;
		this.multiple = value instanceof Collection;
		return this;
	}
	
	public static CustomFieldValue of(int customFieldId, Object value) {
		return new CustomFieldValue(customFieldId, null, value, value instanceof Collection);
	}
}
