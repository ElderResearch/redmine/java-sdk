/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.BiFunction;

import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.structs.ListWithIndex;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Synchronized;
import lombok.val;

@EqualsAndHashCode
public class CustomFieldValues implements HasCustomValues, Serializable {
	@Getter
	private ListWithIndex<CustomFieldValue, Integer> customFields;
	
	@JsonIgnore @Synchronized
	private ListWithIndex<CustomFieldValue, Integer> values() {
		if (customFields == null) { customFields = new ListWithIndex<>(); }
		// Ensure the key extractor is always set, even after deserialization
		return customFields.setKeyExtractor(CustomFieldValue::getId);
	}
	
	@JsonIgnore
	@Override
	public void setCustomValue(int customFieldId, Object value) {
		mergeCustomValue(customFieldId, value, ($, v) -> v);
	}
	
	@Override
	public void addCustomValue(int customFieldId, Object value) {
		mergeCustomValue(customFieldId, value, (prev, v) -> {
			Collection<Object> c;
			if (prev instanceof Collection) {
				c = Utilities.cast(prev);
			} else {
				c = new LinkedList<>();
				c.add(prev);
			}
			c.add(v);
			return c;
		});
	}
	
	public void addCustomValue(CustomFieldValue cfv) {
		values().add(cfv);
	}
	
	private void mergeCustomValue(int customFieldId, Object value, BiFunction<Object, Object, Object> merger) {
		val cfv = values().getByKey(customFieldId);
		if (cfv == null) {
			customFields.add(CustomFieldValue.of(customFieldId, value));
		} else {
			cfv.setValue(merger.apply(cfv.getValue(), value));
		}
	}
	
	@JsonIgnore
	@Override
	public Object getCustomValue(int customFieldId) {
		if (customFields == null) { return null; }
		
		return LambdaUtils.apply(values().getByKey(customFieldId), CustomFieldValue::getValue);
	}
	
	public void clearCustomValues() {
		customFields = null;
	}
}
