/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

public interface HasCustomValues {
	void setCustomValue(int customFieldId, Object value);
	void addCustomValue(int customFieldId, Object value);
	Object getCustomValue(int customFieldId);
}
