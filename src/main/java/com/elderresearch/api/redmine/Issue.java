/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import static com.elderresearch.commons.lang.LambdaUtils.apply;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.elderresearch.commons.rest.client.Identifiable;
import com.elderresearch.commons.structs.ListWithIndex;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Synchronized;
import lombok.val;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true, exclude = {"privateNotes", "notes"})
@Accessors(chain = true)
public class Issue extends CustomFieldValues {
	private Integer id;
	private Integer parentIssueId;

	private String subject;
	private String description;
	private String startDate;
	private String dueDate;
	
	private Integer projectId;
	private Integer trackerId;
	private Integer statusId;
	private Integer priorityId;
	private Integer categoryId;
	private Integer authorId;
	private Integer assignedToId;
	private Integer fixedVersionId;
	
	private Date createdOn;
	private Date updatedOn;
	
	private String notes;
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	private Boolean privateNotes;

	// Only applies if include=relations
	private ListWithIndex<IssueRelation, Integer> relations;
	
	// Only applies if include=children is provided on single issues by ID
	private List<IssueChild> children;
	
	private Set<Integer> watchers;

	@JsonIgnore @Synchronized
	private ListWithIndex<IssueRelation, Integer> relations() {
		if (relations == null) { relations = new ListWithIndex<>(); }
		// Ensure the key extractor is always set, even after deserialization
		return relations.setKeyExtractor(r ->
			Objects.equals(r.getIssueId(), getId())? r.getIssueToId() : r.getIssueId());
	}
	
	@JsonIgnore
	public IssueRelation getRelationTo(int issueId) {
		return relations().getByKey(issueId);
	}
	
	/**
	 * Adds a link to the other issue with the specified type, even if a link already exists to that
	 * issue. This method is faster than {@link #addRelation(int, IssueRelationType)} since the cached maps to optimize
	 * {@link #getRelationTo(int)} are not initialized between adds.
	 * @param issueId the "to" issue ID
	 * @param type the relation type
	 */
	public void addRelationUnsafe(int issueId, IssueRelationType type) {
		relations().add(type.from(getId()).to(issueId));
	}
	
	/**
	 * Adds a link to the other issue if no link already exists. This safer method is slower than
	 * {@link #addRelationUnsafe(int, IssueRelationType)} since the cached maps to optimize {@link #getRelationTo(int)}
	 * are initialized for each call to this method.
	 * @param issueId the "to" issue ID
	 * @param type the relation type
	 * @return if a relation was added or {@code false} if one already existed
	 */
	public boolean addRelation(int issueId, IssueRelationType type) {
		val existing = getRelationTo(issueId);
		if (existing == null) {
			addRelationUnsafe(issueId, type);
			return true;
		}
		return false;
	}
	
	public boolean addWatcher(int watcher) {
		if (watchers == null) { watchers = new HashSet<>(); }
		return watchers.add(watcher);
	}
	
	public Issue setWatchers(Set<Identifiable> watchers) {
		this.watchers = new HashSet<>();
		watchers.forEach($ -> this.watchers.add($.getId()));
		return this;
	}
	
	public Issue setWatcherIds(Set<Integer> watchers) {
		this.watchers = watchers;
		return this;
	}
		
	public Issue setProject(Identifiable i)      { return setProjectId(apply(i, Identifiable::getId)); }
	public Issue setTracker(Identifiable i)      { return setTrackerId(apply(i, Identifiable::getId)); }
	public Issue setStatus(Identifiable i)       { return setStatusId(apply(i, Identifiable::getId)); }
	public Issue setPriority(Identifiable i)     { return setPriorityId(apply(i, Identifiable::getId)); }
	public Issue setCategory(Identifiable i)     { return setCategoryId(apply(i, Identifiable::getId)); }
	public Issue setAuthor(Identifiable i)       { return setAuthorId(apply(i, Identifiable::getId)); }
	public Issue setAssignedTo(Identifiable i)   { return setAssignedToId(apply(i, Identifiable::getId)); }
	public Issue setFixedVersion(Identifiable i) { return setFixedVersionId(apply(i, Identifiable::getId)); }
}
