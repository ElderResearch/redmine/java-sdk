/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class IssueCategory extends Identifiable {
	private Identifiable project;
	private Identifiable assignedTo;
	
	public static IssueCategory of(String name) {
		val ret = new IssueCategory();
		ret.setName(name);
		return ret;
	}
}
