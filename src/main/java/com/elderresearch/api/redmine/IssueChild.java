/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Getter;

@Getter
public class IssueChild {
	private Integer id;
	private String summary;
	private Identifiable tracker;
}
