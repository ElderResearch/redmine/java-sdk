/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.api.redmine.util.IssueRelationPost;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IssueRelation {
	private Integer id;
	private Integer issueId;
	private Integer issueToId;
	private IssueRelationType relationType;
	private Integer delay;
	
	public IssueRelation to(Integer toId) {
		return setIssueToId(toId);
	}

	public IssueRelationPost post() {
		return new IssueRelationPost().setIssueId(issueId).setRelation(this).persist(IssueRelationPost.class);
	}

}
