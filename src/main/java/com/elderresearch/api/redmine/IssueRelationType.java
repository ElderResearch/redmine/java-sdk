/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.commons.lang.Utilities;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The different issue relationship types in Redmine. These are baked in to the Redmine source because each relationship
 * type requires different workflow logic, so they aren't customizable like other enumerations.
 * 
 *
 * @since Nov 8, 2017
 */
public enum IssueRelationType {
	RELATES,
	DUPLICATES,
	DUPLICATED,
	BLOCKS,
	BLOCKED,
	PRECEDES,
	FOLLOWS,
	COPIED_TO,
	COPIED_FROM;
	
	public IssueRelation from(int issueId) {
		return new IssueRelation().setIssueId(issueId).setRelationType(this);
	}

	@JsonValue
	public String nameLowercase() { return name().toLowerCase(); }
	
	@JsonCreator
	public static IssueRelationType fromString(String s) {
		return Utilities.valueOf(IssueRelationType.class, s, RELATES);
	}
}
