/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class IssueStatus extends Identifiable {
	private boolean isDefault;
	private boolean isClosed;
}
