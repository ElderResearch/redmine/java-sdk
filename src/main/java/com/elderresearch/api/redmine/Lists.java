/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.elderresearch.api.redmine.plugins.gamification.Badge;
import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;

public interface Lists {
	interface HasListOf<T> extends Serializable {
		List<T> list();
	}
	
	@Data
	abstract class ListWithTotal<T> implements HasListOf<T> {
		private int totalCount;
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	abstract class ListWithPagination<T> extends ListWithTotal<T> {
		private int offset;
		private int limit;
	}
	
	@Data
	class CustomFieldList implements HasListOf<CustomField> {
		private List<CustomField> customFields = Collections.emptyList();
		
		@Override public List<CustomField> list() { return getCustomFields(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class IssueList extends ListWithPagination<Issue> {
		private List<Issue> issues;
		
		@Override public List<Issue> list() { return getIssues(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class IssueCategoryList extends ListWithTotal<IssueCategory> {
		private List<IssueCategory> issueCategories = Collections.emptyList();
		
		@Override public List<IssueCategory> list() { return getIssueCategories(); }
	}
	
	@Data
	class IssueStatusList implements HasListOf<IssueStatus> {
		private List<IssueStatus> issueStatuses = Collections.emptyList();
		
		@Override public List<IssueStatus> list() { return getIssueStatuses(); }
	}
	
	@Data
	class IssuePriorityList implements HasListOf<IssuePriority> {
		private List<IssuePriority> issuePriorities = Collections.emptyList();
		
		@Override public List<IssuePriority> list() { return getIssuePriorities(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class TimeEntryList extends ListWithTotal<TimeEntry> {
		private List<TimeEntry> timeEntries = Collections.emptyList();
		
		@Override public List<TimeEntry> list() { return getTimeEntries(); }
	}
	
	@Data
	class TimeEntryActivityList implements HasListOf<TimeEntryActivity> {
		private List<TimeEntryActivity> timeEntryActivities = Collections.emptyList();
		
		@Override public List<TimeEntryActivity> list() { return getTimeEntryActivities(); }
	}
	
	@Data
	class TrackerList implements HasListOf<Identifiable> {
		private List<Identifiable> trackers = Collections.emptyList();
		
		@Override public List<Identifiable> list() { return getTrackers(); }
	}
	
	@Data
	class ProjectFileList implements HasListOf<Upload> {
		private List<Upload> files = Collections.emptyList();
		
		@Override
		public List<Upload> list() { return files; }
	}

	@Data
	class ProjectList implements HasListOf<Project> {
		private List<Project> projects = Collections.emptyList();
		
		@Override
		public List<Project> list() { return projects; }
	}
	
	@Data
	class RoleList implements HasListOf<Identifiable> {
		private List<Identifiable> roles = Collections.emptyList();
		
		@Override public List<Identifiable> list() { return getRoles(); }
	}
	
	@Data
	class IssueRelationList implements HasListOf<IssueRelation> {
		private List<IssueRelation> relations = Collections.emptyList();
		
		@Override public List<IssueRelation> list() { return getRelations(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class UserList extends ListWithPagination<User> {
		private List<User> users = Collections.emptyList();
		
		@Override public List<User> list() { return getUsers(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class AuthSourceList extends ListWithPagination<AuthSource> {
		private List<AuthSource> authSources = Collections.emptyList();
		
		@Override public List<AuthSource> list() { return getAuthSources(); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	class UserMembershipList extends ListWithPagination<UserMembership> {
		private List<UserMembership> memberships = Collections.emptyList();
		
		@Override
		public List<UserMembership> list() { return getMemberships(); }
	}
	
	@Data
	class WikiPageList implements HasListOf<WikiPage> {
		private List<WikiPage> wikiPages = Collections.emptyList();
		
		@Override
		public List<WikiPage> list() { return getWikiPages(); }
	}

	@Data @EqualsAndHashCode(callSuper = true)
	class SearchResultList extends ListWithPagination<SearchResult> {
		private List<SearchResult> results = Collections.emptyList();
		
		@Override
		public List<SearchResult> list() { return getResults(); }
	}

	//Todo: move this into plugin Lists.java
	@Data @EqualsAndHashCode(callSuper = true)
	class BadgeList extends ListWithTotal<Badge> {
		private List<Badge> badges = Collections.emptyList();

		@Override public List<Badge> list() { return getBadges(); }
	}
}
