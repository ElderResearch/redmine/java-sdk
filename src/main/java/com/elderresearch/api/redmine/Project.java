/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.util.Date;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Project extends Identifiable {
	private Integer parentId;
	private String identifier;
	private String description;
	private String homepage;
	private Date createdOn;
	private Date updatedOn;
	private Boolean isPublic;
}
