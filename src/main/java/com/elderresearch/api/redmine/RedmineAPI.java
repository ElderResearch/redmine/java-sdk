/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import static com.elderresearch.commons.lang.Utilities.cast;
import static com.elderresearch.commons.rest.client.RecursiveTarget.newTarget;

import java.io.InputStream;
import java.util.List;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.elderresearch.api.redmine.Lists.AuthSourceList;
import com.elderresearch.api.redmine.Lists.CustomFieldList;
import com.elderresearch.api.redmine.Lists.IssueCategoryList;
import com.elderresearch.api.redmine.Lists.IssuePriorityList;
import com.elderresearch.api.redmine.Lists.IssueRelationList;
import com.elderresearch.api.redmine.Lists.IssueStatusList;
import com.elderresearch.api.redmine.Lists.ProjectFileList;
import com.elderresearch.api.redmine.Lists.ProjectList;
import com.elderresearch.api.redmine.Lists.RoleList;
import com.elderresearch.api.redmine.Lists.TimeEntryActivityList;
import com.elderresearch.api.redmine.Lists.TrackerList;
import com.elderresearch.api.redmine.Lists.WikiPageList;
import com.elderresearch.api.redmine.Upload.UploadResponse;
import com.elderresearch.api.redmine.plugins.IssueWithTags.IssueWithTagsPaginator;
import com.elderresearch.api.redmine.util.AttachmentPatch;
import com.elderresearch.api.redmine.util.IssueCategoryPost;
import com.elderresearch.api.redmine.util.IssuePost;
import com.elderresearch.api.redmine.util.IssuePut;
import com.elderresearch.api.redmine.util.IssuePut.IssuePutBase;
import com.elderresearch.api.redmine.util.IssueRelationPost;
import com.elderresearch.api.redmine.util.ProjectFilePost;
import com.elderresearch.api.redmine.util.ProjectPost;
import com.elderresearch.api.redmine.util.RedminePaginator.SearchPaginator;
import com.elderresearch.api.redmine.util.RedminePaginator.UserPaginator;
import com.elderresearch.api.redmine.util.TimeEntryPost;
import com.elderresearch.api.redmine.util.TimeEntryPut;
import com.elderresearch.api.redmine.util.UserMembershipPost;
import com.elderresearch.api.redmine.util.UserMembershipPost.Membership;
import com.elderresearch.api.redmine.util.UserPost;
import com.elderresearch.api.redmine.util.UserPut;
import com.elderresearch.api.redmine.util.WatcherAdd;
import com.elderresearch.api.redmine.util.WikiPagePut;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.rest.client.Identifiable;
import com.elderresearch.commons.rest.client.RecursiveTarget;
import com.elderresearch.commons.rest.client.RestEndpoint;
import com.elderresearch.commons.rest.client.RestEndpoint.RestEndpointWithIdInt;
import com.elderresearch.commons.rest.client.RestEndpoint.RestEndpointWithIdString;
import com.elderresearch.commons.rest.client.WebParam;
import com.elderresearch.commons.rest.client.WebParam.WebHeader;
import com.elderresearch.commons.rest.client.WebParam.WebParamGroup;
import com.elderresearch.commons.rest.client.WebParam.WebQueryParam;
import com.elderresearch.commons.rest.client.WebParam.WebTemplateParam;
import com.google.common.base.Supplier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import lombok.experimental.Delegate;

@Getter @Accessors(fluent = true)
public class RedmineAPI {
	private final RedmineClient client;
	
	public Invocation.Builder request(String path, WebParam... params) {
		return client().request(newTarget(path), params);
	}
	
	private final IssuesAPI              issues;
	private final IssueAPI               issue;
	private final IssueRelationsAPI      issueRelations;
	private final RelationsAPI           relations;
	private final IssueWatchersAPI       watchers;
	private final TimeEntriesAPI         timeEntries;
	private final TimeEntryAPI           timeEntry;
	private final ProjectsAPI            projects;
	private final ProjectAPI             project;
	private final ProjectMembershipAPI   projectMembership;
	private final ProjectFilesAPI        projectFiles;
	private final IssueCategoriesAPI     issueCategories;
	private final IssueCategoryAPI       issueCategory;
	private final IssueStatusesAPI       issueStatuses;
	private final TrackersAPI            trackers;
	private final CustomFieldsAPI        customFields;
	private final RecursiveTarget        enumerations = newTarget("enumerations");
	private final IssuePrioritiesAPI     issuePriorities;
	private final TimeEntryActivitiesAPI timeEntryActivities;
	private final WikiPagesAPI           wikiPages;
	private final AttachmentsAPI         attachments;
	private final UploadsAPI             uploads;
	private final RolesAPI               roles;
	private final UsersAPI               users;
	private final UserAPI                user;
	private final AuthSourcesAPI         authSources;
	private final SearchAPI              search;
	
	// Must initialize API objects after client field is set, and order matters, since some APIs refer to others
	public RedmineAPI(RedmineClient client) {
		this.client = client;
		
		this.issues              = new IssuesAPI();
		this.issue               = new IssueAPI();
		this.issueRelations      = new IssueRelationsAPI();
		this.relations           = new RelationsAPI();
		this.watchers            = new IssueWatchersAPI();
		this.timeEntries         = new TimeEntriesAPI();
		this.timeEntry           = new TimeEntryAPI();
		this.projects            = new ProjectsAPI();
		this.project             = new ProjectAPI();
		this.projectMembership   = new ProjectMembershipAPI();
		this.projectFiles        = new ProjectFilesAPI();
		this.issueCategories     = new IssueCategoriesAPI();
		this.issueCategory       = new IssueCategoryAPI();
		this.issueStatuses       = new IssueStatusesAPI();
		this.trackers            = new TrackersAPI();
		this.customFields        = new CustomFieldsAPI();
		this.issuePriorities     = new IssuePrioritiesAPI();
		this.timeEntryActivities = new TimeEntryActivitiesAPI();
		this.wikiPages           = new WikiPagesAPI();
		this.attachments         = new AttachmentsAPI();
		this.uploads             = new UploadsAPI();
		this.roles               = new RolesAPI();
		this.users               = new UsersAPI();
		this.user                = new UserAPI();
		this.authSources         = new AuthSourcesAPI();
		this.search              = new SearchAPI();
	}
	
	public interface KeyHeader {
		static WebHeader of(String apiKey) {
			return WebHeader.of("X-Redmine-API-Key", apiKey);
		}
	}
	
	public static class PageParams extends WebParamGroup {
		private PageParams(long offset, long limit) {
			super(WebQueryParam.of("offset", offset), WebQueryParam.of("limit", limit));
		}
		public static PageParams of(long offset, long limit) {
			return new PageParams(offset, limit);
		}
	}
	
	public static class ProjectParam extends WebQueryParam {
		private ProjectParam(Object value) { super("project_id", value); }
		public static ProjectParam of(int... ids) { return new ProjectParam(StringUtils.join(ids, '|')); }
	}
	
	public static class TrackerParam extends WebQueryParam {
		private TrackerParam(Object value) { super("tracker_id", value); }
		public static TrackerParam of(int... ids) { return new TrackerParam(StringUtils.join(ids, '|')); }
	}
	
	public static class StatusParam extends WebQueryParam {
		private StatusParam(Object value) { super("status_id", value); }
		public static StatusParam of(int... ids) { return new StatusParam(StringUtils.join(ids, '|')); }
		public static StatusParam all() { return new StatusParam("*"); }
	}
	
	public static class PriorityParam extends WebQueryParam {
		private PriorityParam(Object value) { super("priority_id", value); }
		public static PriorityParam of(int... ids) { return new PriorityParam(StringUtils.join(ids, '|')); }
	}
	
	public static class CustomFieldParam extends WebQueryParam {
		private CustomFieldParam(int id, Object value) { super("cf_" + id, value); }
		public static CustomFieldParam of(int cfId, Object value) { return new CustomFieldParam(cfId, value); }
	}
	
	public static class IncludeParam extends WebQueryParam {
		public static final String
			RELATIONS = "relations",
			ATTACHMENTS = "attachments",
			CHILDREN = "children",
			CHANGESETS = "changesets",
			JOURNALS = "journals",
			WATCHERS = "watchers";
		
		private IncludeParam(String... includes) { super("include", (Object[]) includes); }
		public static IncludeParam of(String... includes) { return new IncludeParam(includes); }
		
		public static IncludeParam relations()   { return of(RELATIONS); }
		public static IncludeParam attachments() { return of(ATTACHMENTS); }
		public static IncludeParam all()         { return of(ATTACHMENTS, RELATIONS, CHILDREN, CHANGESETS, JOURNALS, WATCHERS); }
	}
	
	public class IssuesAPI extends RestEndpoint {
		IssuesAPI() { super(client, newTarget("issues")); }
		
		public <I extends Issue> IssuePost<I> newPost(I i) {
			val ret = new IssuePost<I>();
			ret.setIssue(i).to(RedmineAPI.this);
			return ret;
		}
		
		public IssueWithTagsPaginator paginator(WebParam... params) {
			return new IssueWithTagsPaginator(RedmineAPI.this, params);
		}
	}
	
	public class IssueAPI extends RestEndpointWithIdInt {
		IssueAPI() { super(client, issues.target().child(idTemplate())); }
		
		public <I extends Issue> I get(int id, WebParam... params) {
			return LambdaUtils.apply(request(id, params).get(new GenericType<IssuePost<I>>() {
				// Anonymous class for reflection
			}), IssuePost::getIssue);
		}
		
		public <I extends Issue> IssuePut<I> newPut(Issue i) {
			return cast(new IssuePutBase(i).to(RedmineAPI.this));
		}
		
		public <I extends Issue> IssuePut<I> newPut(Supplier<I> issueFactory, I i) {
			return cast(new IssuePut<I>(issueFactory, i) {
				// Anonymous class for reflection
			}.to(RedmineAPI.this));
		}
	}
	
	public class IssueRelationsAPI extends RestEndpointWithIdInt {
		IssueRelationsAPI() { super(client, issue.target().child("relations")); }
		
		public List<IssueRelation> getAll(int issueId) {
			return request(issueId).get(IssueRelationList.class).getRelations();
		}
		
		public IssueRelationPost newPost(int issueId, IssueRelation r) {
			return cast(new IssueRelationPost().setIssueId(issueId).setRelation(r).to(RedmineAPI.this));
		}
	}
	
	public class RelationsAPI extends RestEndpointWithIdInt {
		RelationsAPI() { super(client, newTarget("relations").child(idTemplate())); }
	}
	
	public class IssueWatchersAPI {
		private final RecursiveTarget target = issue.target().child("watchers");
		
		public WatchersForIssue forIssue(int id) {
			return new WatchersForIssue(target, id);
		}
	}
	
	public class WatchersForIssue extends RestEndpointWithIdInt {
		private final int issueId;
		private final RecursiveTarget userTarget;
		
		WatchersForIssue(RecursiveTarget t, int issueId) {
			super(client, t);
			this.issueId = issueId;
			this.userTarget = t.child("{userId}");
		}
		
		@Override
		public Invocation.Builder request(WebParam... params) {
			return request(issueId, params);
		}
		
		public Invocation.Builder forUser(int userId, WebParam... params) {
			return client().request(userTarget, ArrayUtils.addAll(params, idParam(issueId), WebTemplateParam.of("userId", userId)));	
		}
		
		public WatcherAdd add(int userId) {
			return cast(new WatcherAdd(issueId).setUserId(userId).to(RedmineAPI.this));
		}
		
		public Response delete(int userId) {
			return forUser(userId).delete();
		}
	}
	
	public class TimeEntriesAPI extends RestEndpoint {
		TimeEntriesAPI() { super(client, newTarget("time_entries")); }
			
		public TimeEntryPost newPost(TimeEntry t) {
			return cast(new TimeEntryPost().setTimeEntry(t).to(RedmineAPI.this));
		}
	}
	
	public class TimeEntryAPI extends RestEndpointWithIdInt {
		TimeEntryAPI() { super(client, timeEntries.target().child(idTemplate())); }
		
		public TimeEntryPut newPut(TimeEntry t) {
			return cast(new TimeEntryPut(t).to(RedmineAPI.this));
		}
	}
	
	public class ProjectsAPI extends RestEndpoint {
		ProjectsAPI() { super(client, newTarget("projects")); }
		
		public List<Project> getAll(WebParam... params) {
			return request(params).get(ProjectList.class).getProjects();
		}
		
		public ProjectPost newPost(Project p) {
			return cast(new ProjectPost().setProject(p).to(RedmineAPI.this));
		}
	}
	
	public class ProjectAPI extends RestEndpointWithIdString {
		ProjectAPI() { super(client, projects.target().child(idTemplate())); }
		
		public Project get(String id, WebParam... params) {
			return LambdaUtils.apply(request(id, params).get(ProjectPost.class), ProjectPost::getProject);
		}
	}
	
	public class ProjectMembershipAPI extends RestEndpointWithIdInt {
		ProjectMembershipAPI() { super(client, project.target().child("memberships")); }
		
		public UserMembershipPost newPost(int projectId, int userId, int... roleIds) {
			val m = Membership.of(userId, roleIds);
			return cast(new UserMembershipPost().setProjectId(projectId).setMembership(m).to(RedmineAPI.this));
		}
	}
	
	public class ProjectFilesAPI extends RestEndpointWithIdInt {
		ProjectFilesAPI() { super(client, project.target().child("files")); }
		
		public List<Upload> getAll(int projectId) {
			return request(projectId).get(ProjectFileList.class).getFiles();
		}
		
		public ProjectFilePost newPost(int projectId, Upload u) {
			return cast(new ProjectFilePost().setProjectId(projectId).setFile(u).to(RedmineAPI.this));
		}
	}
	
	public class IssueCategoriesAPI extends RestEndpointWithIdInt {
		IssueCategoriesAPI() { super(client, project.target().child("issue_categories")); }
		
		public List<IssueCategory> getAll(int projectId) {
			return request(projectId).get(IssueCategoryList.class).getIssueCategories();
		}
		
		public IssueCategoryPost newPost(int projectId, IssueCategory c) {
			return cast(new IssueCategoryPost().setProjectId(projectId).setIssueCategory(c).to(RedmineAPI.this));
		}
	}
	
	public class IssueCategoryAPI extends RestEndpointWithIdInt {
		IssueCategoryAPI() { super(client, newTarget("issue_categories").child(idTemplate())); }
	}
	
	public class IssueStatusesAPI extends RestEndpoint {
		IssueStatusesAPI() { super(client, newTarget("issue_statuses")); }
		
		public List<IssueStatus> getAll() {
			return request().get(IssueStatusList.class).getIssueStatuses();
		}
	}
	
	public class IssuePrioritiesAPI extends RestEndpoint {
		IssuePrioritiesAPI() { super(client, enumerations.child("issue_priorities")); }
		
		public List<IssuePriority> getAll() {
			return request().get(IssuePriorityList.class).getIssuePriorities();
		}
	}
	
	public class TimeEntryActivitiesAPI extends RestEndpoint {
		TimeEntryActivitiesAPI() { super(client, enumerations.child("time_entry_activities")); }
		
		public List<TimeEntryActivity> getAll() {
			return request().get(TimeEntryActivityList.class).getTimeEntryActivities();
		}
	}
	
	public class TrackersAPI extends RestEndpoint {
		TrackersAPI() { super(client, newTarget("trackers")); }
		
		public List<Identifiable> getAll() {
			return request().get(TrackerList.class).getTrackers();
		}
	}
	
	public class CustomFieldsAPI extends RestEndpoint {
		CustomFieldsAPI() { super(client, newTarget("custom_fields")); }
		
		public List<CustomField> getAll() {
			return request().get(CustomFieldList.class).getCustomFields();
		}
	}
	
	public class RolesAPI extends RestEndpoint {
		RolesAPI() { super(client, newTarget("roles")); }
		
		public List<Identifiable> getAll() {
			return request().get(RoleList.class).getRoles();
		}
	}
	
	public class UsersAPI extends RestEndpoint {
		UsersAPI() { super(client, newTarget("users")); }
		
		public UserPut newPost(User u) {
			return cast(new UserPost().setUser(u).to(RedmineAPI.this));
		}
		
		public UserPaginator paginator(WebParam... params) {
			return new UserPaginator(RedmineAPI.this, params);
		}
	}
	
	public class UserAPI extends RestEndpointWithIdInt {
		UserAPI() { super(client, users.target().child(idTemplate())); }
		
		public UserPut newPut(User u) {
			return cast(new UserPut(u).to(RedmineAPI.this));
		}

		public UserPost newPost(User u) {
            UserPost userPost = new UserPost();
            userPost.setUser(u);
            return cast(userPost.to(RedmineAPI.this));
        }
	}
	
	public class WikiPagesAPI extends RestEndpointWithIdInt {
		WikiPagesAPI() { super(client, project.target().child("wiki")); }
		
		public Invocation.Builder request(int projectId, String title, WebParam... params) {
			return client().request(target().child(title), ArrayUtils.add(params, idParam(projectId)));
		}
		
		public List<WikiPage> getAll(int projectId) {
			return request(projectId, "index").get(WikiPageList.class).getWikiPages();
		}
		
		public WikiPagePut newPut(int projectId, WikiPage p) {
			return cast(new WikiPagePut().setProjectId(projectId).setWikiPage(p).to(RedmineAPI.this));
		}
	}
	
	public class AttachmentsAPI extends RestEndpointWithIdInt {
		AttachmentsAPI() { super(client, newTarget("attachments").child(idTemplate())); }
		
		public AttachmentPatch newPatch(Upload u) {
			return cast(new AttachmentPatch(u).to(RedmineAPI.this));
		}
	}
	
	public class UploadsAPI extends RestEndpoint {
		UploadsAPI() { super(client, newTarget("uploads")); }
		
		public Upload upload(InputStream stream) {
			return LambdaUtils.apply(request().post(
				Entity.entity(stream, MediaType.APPLICATION_OCTET_STREAM_TYPE)
			).readEntity(UploadResponse.class), UploadResponse::getUpload);
		}
	}
	
	// NOTE: Requires customizations to Redmine 3.4 to expose auth sources via API
	public class AuthSourcesAPI extends RestEndpoint {
		AuthSourcesAPI() { super(client, newTarget("auth_sources")); }
		
		public List<AuthSource> getAll() {
			return request().get(AuthSourceList.class).getAuthSources();
		}
	}
	
	// NOTE: Undocumented but apparently "official" https://www.redmine.org/projects/redmine/wiki/Rest_Search
	@AllArgsConstructor
	public enum SearchOption implements WebParam {
		ALL_WORDS,
		TITLES_ONLY,
		
		// Which items to search
		ISSUES,
		DOCUMENTS,
		WIKI_PAGES,
		MESSAGES,
		CONTACTS,
		QUESTIONS,
		
		OPEN_ISSUES,
		
		// Exclusive
		ATTACHMENTS,
		ATTACHMENTS_ONLY(WebQueryParam.of("attachments", "only")),
		
		// Exclusive
		ALL_PROJECTS(WebQueryParam.of("scope", "all")),
		MY_PROJECTS(WebQueryParam.of("scope", "my_projects"));
		
		@Delegate
		private WebParam param;
		
		private SearchOption() {
			param = WebQueryParam.of(name().toLowerCase(), "1");
		}
	}
	
	public class SearchAPI extends RestEndpoint {
		SearchAPI() { super(client, newTarget("search")); }
		
		public SearchPaginator paginator(WebParam... params) {
			return new SearchPaginator(RedmineAPI.this, params);
		}
		
		public SearchPaginator query(String q, WebParam... options) {
			return paginator(ArrayUtils.add(options, WebQueryParam.of("q", q)));
		}
		
		public SearchResult first(String q, WebParam... options) {
			return Utilities.first(query(q, options).page(0L, 1L));
		}
	}
}
