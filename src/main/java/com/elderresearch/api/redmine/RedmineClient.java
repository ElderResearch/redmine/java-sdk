/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import jakarta.ws.rs.client.Invocation.Builder;

import com.elderresearch.api.redmine.util.RedmineFormat;
import com.elderresearch.commons.rest.client.RecursiveTarget;
import com.elderresearch.commons.rest.client.RestClient;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
public class RedmineClient extends RestClient {
	// All endpoints expect a "extension" and since we're using Jackson, we always want JSON
	private static final String FORMAT = ".json";
	
	@Getter
	private static final ObjectMapper objectMapper;
	static {
		// Mapping to APIs is not exhaustive in some cases- don't throw an error
		objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		// Redmine doesn't like numerical (s or ms since epoch) dates, which is the default for Jackson
		// TODO: Does this break any APIs that require a time component?
		objectMapper.setConfig(objectMapper.getSerializationConfig().with(new SimpleDateFormat(RedmineFormat.DATE_FORMAT)));
		
		// Jackson defaults to UTC, which means dates are converted to the timezone of the server running the client.
		// Set the default to the system default so dates are left unconverted.
		objectMapper.setTimeZone(TimeZone.getDefault());
		
		// Set default naming strategy to snake so we avoid this annotation on all domain classes
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		
		// When putting updated objects, we commonly will leave some fields null- don't send those so they are unchanged
		objectMapper.setDefaultPropertyInclusion(Include.NON_NULL);
	}
	
	@Getter private final RedmineAPI api;
	
	public RedmineClient(RedmineConfig config) {
		super(config, objectMapper, true);
		this.api = new RedmineAPI(this);
	}
	
	@Override
	public Builder request(RecursiveTarget target, WebParam... params) {
		return super.request(target.append(FORMAT), params);
	}
}
