/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import com.elderresearch.api.redmine.RedmineAPI.KeyHeader;
import com.elderresearch.commons.lang.config.EnvironmentTree;
import com.elderresearch.commons.rest.client.RestClientConfig;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Configuration for the Redmine client. There are four ways of setting these keys:<ol>
 * <li>Using a .yaml file named {@code redmine.yaml} in the current directory</li>
 * <li>Using a .yaml file at a path/location you specify via {@link #load(String, boolean)}</li>
 * <li>Using system properties (e.g. {@code -Dredmine_key=mykey})</li>
 * <li>Using environment variables (e.g. {@code REDMINE_KEY=mykey})</li>
 * </ol>
 * 
 *
 * @since Nov 7, 2017
 */
@Getter @Setter @Accessors(chain = true)
public class RedmineConfig extends RestClientConfig {
	private String key;
	
	@Override
	public WebParam asAuthParam() {
		return KeyHeader.of(getKey());
	}
	
	public void loadDefaultFile() {
		load("redmine.yaml");
	}
	
	public void load(String path) {
		load(EnvironmentTree.forPrefix("redmine"), path);
	}
}
