/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SearchResult {
	private int id;
	private String title;
	private String type;
	private String url;
	private String description;
	private Date datetime;
}
