/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import static com.elderresearch.commons.lang.LambdaUtils.apply;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TimeEntry extends CustomFieldValues {
	private static final int MAX_COMMENTS_LEN = 255;
	
	private Integer id;
	private Integer projectId;
	private Integer issueId;
	private Date spentOn;
	private Float hours;
	private Integer activityId;
	private String comments;
	private Integer userId;

	public TimeEntry setProject(Identifiable i)  { return setProjectId(apply(i, Identifiable::getId)); }
	public TimeEntry setActivity(Identifiable i) { return setActivityId(apply(i, Identifiable::getId)); }
	public TimeEntry setUser(Identifiable i)     { return setUserId(apply(i, Identifiable::getId)); }
	
	public TimeEntry setComments(String comments) {
		this.comments = abbreviateComments(comments);
		return this;
	}
	
	public static String abbreviateComments(String comments) {
		return StringUtils.abbreviate(comments, MAX_COMMENTS_LEN);
	}
}
