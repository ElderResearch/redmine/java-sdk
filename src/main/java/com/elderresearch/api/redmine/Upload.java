/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import static com.elderresearch.commons.lang.LambdaUtils.apply;

import java.util.Date;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Upload {
	private Integer id;
	private String filename;
	private String filesize;
	private String contentType;
	private String description;
	private String contentUrl;
	private Integer authorId;
	private Date createdOn;
	private Integer versionId;
	private String digest;
	private Integer downloads;
	private String token;
	
	public Upload setAuthor(Identifiable i)  { return setAuthorId(apply(i, Identifiable::getId)); }
	public Upload setVersion(Identifiable i) { return setVersionId(apply(i, Identifiable::getId)); }
	
	@Getter
	public static class UploadResponse {
		private Upload upload;
	}
}
