/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.commons.rest.client.Identifiable;
import com.elderresearch.commons.rest.client.WebParam.WebQueryParam;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class User extends CustomFieldValues {
	public enum UserStatus {
		ANONYMOUS,
		ACTIVE,
		REGISTERED,
		LOCKED;
		
		@JsonValue
		public int toValue() { return ordinal(); }
		
		@JsonCreator
		public static UserStatus valueOf(int i) {
			return values()[i];
		}
		
		public WebQueryParam param() {
			return WebQueryParam.of("status", toValue());
		}
		
		public static WebQueryParam all() {
			return WebQueryParam.of("status", StringUtils.EMPTY);
		}
	}
	
	private Integer id;
	private String login;
	private String password;
	private String firstname;
	private String lastname;
	private String mail;
	private Date createdOn;
	private Date lastLoginOn;
	private UserStatus status;
	private String apiKey;
	private Integer authSourceId;
	private String mailNotification;
	
	// Requires change to Redmine source to add this field
	private Boolean admin;
	
	private List<UserMembership> memberships;
	private List<Identifiable> groups;
	
	@JsonIgnore
	public boolean isLocked() {
		return getStatus() == UserStatus.LOCKED;
	}
}
