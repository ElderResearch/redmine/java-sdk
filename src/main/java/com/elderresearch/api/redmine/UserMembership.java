/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import java.util.Collections;
import java.util.List;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserMembership {
	private Integer id;
	private Identifiable project;
	private Identifiable user;
	private List<Identifiable> roles = Collections.emptyList();
}
