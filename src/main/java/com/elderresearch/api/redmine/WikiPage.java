/* ©2019-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine;

import static com.elderresearch.commons.lang.LambdaUtils.apply;

import java.util.Date;

import com.elderresearch.api.redmine.util.WikiPagePut;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WikiPage {
	private WikiPageTitle parent;
	
	private String title;
	private String text;
	private Integer version;
	
	private Integer authorId;
	
	private String comments;
	private Date updatedOn;
	private Date createdOn;
	
	public WikiPage setAuthor(Identifiable i) { return setAuthorId(apply(i, Identifiable::getId)); }
	
	@Data
	@Accessors(chain = true)
	public static class WikiPageTitle {
		private String title;
	}
	
	public WikiPage put(int projectId) {
		return LambdaUtils.apply(
			new WikiPagePut().setWikiPage(this).setProjectId(projectId).persist(WikiPagePut.class), WikiPagePut::getWikiPage
		);
	}
}
