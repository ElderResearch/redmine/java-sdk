/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.enums;

import java.util.Collection;
import java.util.Objects;

import org.jooq.lambda.Seq;

import com.elderresearch.api.redmine.HasCustomValues;

import lombok.val;

public interface CustomFieldEnum extends RedmineEnum {
	default Object fromRedmineValue(String val) { return val; }
	
	default Object get(HasCustomValues issue) {
		val v = issue.getCustomValue(getId());
		if (v instanceof Collection<?>) {
			return Seq.seq((Collection<?>) v).map(o -> Objects.toString(o, null)).map(this::fromRedmineValue).toList();
		}
		return fromRedmineValue(Objects.toString(v, null));
	}

	default String toRedmineValue(Object val) { return Objects.toString(val, null); }
	
	default void set(HasCustomValues issue, Object value) {
		issue.setCustomValue(getId(), toRedmineValue(value));
	}
}
