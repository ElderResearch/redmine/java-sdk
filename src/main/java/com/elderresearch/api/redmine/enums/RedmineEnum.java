/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.enums;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.lang.Utilities;

public interface RedmineEnum {
	String name();
	
	default String getDisplayName() {
		return Utilities.getFriendlyString(name());
	}
	
	int getId();
	void setId(int id);

	// Override these if your application cares about multiple API scopes and the enum needs to track
	// potentially different IDs for different API instances
	default int getId(RedmineAPI scope) {
		return getId();
	}
	default void setId(RedmineAPI scope, int id) {
		setId(id);
	}
}
