/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.enums;

import java.util.function.Function;
import java.util.function.Supplier;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(fluent = true, chain = true)
public class RedmineEnumMapping<T extends RedmineEnum, I extends Identifiable> {
	private final Class<T> type;
	
	private Supplier<T[]> valuesProvider;
	private Function<I, String> nameProvider = Identifiable::getName;
	
	public RedmineEnumMapping(Class<T> type) {
		this.type = type;
		this.valuesProvider = type::getEnumConstants;
	}
	
	/**
	 * Require all values declared in source code to be mapped to values in Redmine.
	 * @param required if all declared enums must exist in Redmine
	 * @return if all declared enums must exist in Redmine
	 */
	private boolean required = true;
	
	/**
	 * Allow additional values for this enum in Redmine beyond the ones mapped in source code.
	 * @param dynamic if additional values are allowed to exist in Redmine that aren't declared in code
	 * @return if additional values are allowed to exist in Redmine that aren't declared in code
	 */
	private boolean dynamic = false;
}
