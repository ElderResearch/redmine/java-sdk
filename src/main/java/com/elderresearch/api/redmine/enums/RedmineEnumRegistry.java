/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.enums;

import static com.elderresearch.commons.lang.LambdaUtils.accept;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jooq.lambda.Seq;

import com.elderresearch.api.redmine.CustomField;
import com.elderresearch.api.redmine.IssueCategory;
import com.elderresearch.api.redmine.IssuePriority;
import com.elderresearch.api.redmine.IssueStatus;
import com.elderresearch.api.redmine.Project;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.RedmineAPI.IssueCategoriesAPI;
import com.elderresearch.api.redmine.TimeEntryActivity;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.rest.client.Identifiable;
import com.elderresearch.commons.structs.TwoKeyHashMap;
import com.google.common.base.Supplier;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Setter @Accessors(fluent = true)
@RequiredArgsConstructor
public class RedmineEnumRegistry {
	@Getter(AccessLevel.PROTECTED)
	private final RedmineAPI api;
	private final Map<RedmineEnum, Integer> idMap = new HashMap<>();
	
	// This map is populated via side effects of the registry init below and is used to track enums that aren't 
	// mirrored in code (for enum types that allow dynamic/user-created values)
	private final TwoKeyHashMap<Class<? extends RedmineEnum>, String, Integer> dynamic = new TwoKeyHashMap<>();
	
	@Getter private RedmineEnumMapping<?, Project> projects;
	@Getter private RedmineEnumMapping<?, IssueStatus> statuses;
	@Getter private RedmineEnumMapping<?, IssuePriority> priorities;
	@Getter private RedmineEnumMapping<?, TimeEntryActivity> timeActivities;
	@Getter private RedmineEnumMapping<?, Identifiable> trackers;
	@Getter private RedmineEnumMapping<?, CustomField> customFields;
	@Getter private RedmineEnumMapping<?, IssueCategory> categories;
	
	/**
	 * Initialize the registry.
	 * @param project the project to use for the {@link IssueCategoriesAPI}. Its ID will be initialized first
	 * before using it to query the project-scoped APIs.
	 * @return {@code this}
	 * @throws IllegalStateException if there was a problem validating the enums in Redmine
	 */
	public RedmineEnumRegistry initialize(RedmineEnum project) {
		val warnings = new LinkedList<String>();
		// Init projects first so the passed project's ID will be valid
		accept(projects(), $ ->
			init("projects", api.projects()::getAll, $.nameProvider(Project::getIdentifier), warnings));
		accept(statuses(), $ ->
			init("issue statuses", api.issueStatuses()::getAll, $, warnings));
		accept(priorities(), $ ->
			init("issue priorities", api.issuePriorities()::getAll, $, warnings));
		accept(timeActivities(), $ ->
			init("time entry activities", api.timeEntryActivities()::getAll, $, warnings));
		accept(trackers(), $ ->
			init("trackers", api.trackers()::getAll, $, warnings));
		accept(customFields(), $ ->
			init("custom fields", api.customFields()::getAll, $, warnings));
		accept(categories(), $ ->
			init("issue categories", () -> api.issueCategories().getAll(project.getId()), $, warnings));
		
		if (!warnings.isEmpty()) {
			warnings.addFirst("Redmine is not configured correctly:");
			throw new IllegalStateException(StringUtils.join(warnings, System.lineSeparator()));
		}
		return this;
	}
	
	private <T extends RedmineEnum, I extends Identifiable> void init(
			String description,
			Supplier<List<I>> loadAll,
			RedmineEnumMapping<T, I> mapping,
			List<String> warnings) {
		
		log.info("Loading {}...", description);
		val declaredAll = mapping.valuesProvider().get();
		loadAll.get().forEach(i -> {
			val name = mapping.nameProvider().apply(i);
			for (T declared : declaredAll) {
				if (StringUtils.equalsAnyIgnoreCase(name, declared.name(), declared.getDisplayName())) {
					declared.setId(api(), i.getId());
					idMap.put(declared, i.getId());
					return;
				}
			}
			
			if (mapping.dynamic()) {
				dynamic.put(mapping.type(), name, i.getId());
			} else {
				warnings.add(String.format("%s '%s' will be unused/ignored", mapping.type().getSimpleName(), name));
			}
		});
		
		if (mapping.required()) {
			Seq.of(declaredAll)
				.filter($ -> !idMap.containsKey($))
				.map($ -> String.format("You must define %s '%s'", mapping.type().getSimpleName(), $.getDisplayName()))
				.forEach(warnings::add);
		}
	}
	
	/**
	 * Finds the enum with the given ID.
	 * @param c the enum type
	 * @param id the ID
	 * @param <T> the specific {@link RedmineEnum} subclass/type
	 * @return the enum with the type and ID or {@code null} if no matching enum is found
	 */
	public <T extends RedmineEnum> T find(Class<T> c, int id) {
		return Utilities.cast(idMap.entrySet().stream()
			.filter(e -> e.getKey().getClass() == c && Objects.equals(e.getValue(), id))
			.findAny()
			.map(Map.Entry::getKey)
			.orElse(null));
	}
	
	/**
	 * Finds a dynamic/user-created enum with the given name. This is for enums that aren't declared or tracked
	 * explicitly in the code, but are allowed to have additional values as configured by a Redmine admin (for example,
	 * the workflow defined by other {@link IssueStatus} values can be changed on the fly). This provides a mechanism
	 * for looking up the ID of these dynamic enum values if you know the name. Note that any enum explicitly
	 * declared in a {@link RedmineEnum} class will <b>not</b> be returned by this method.
	 * @param c the enum type
	 * @param name the enum name
	 * @return the ID of the enum, or {@code null} if no enum value was found for the type with the name
	 */
	public Integer findDynamic(Class<? extends RedmineEnum> c, String name) {
		return dynamic.get(c, name);
	}

	/**
	 * Finds either a dynamic/user-created enum with the given name, or the "system" enum declared explicitly in the
	 * code, and returns the ID.
	 * @param c the enum type
	 * @param name the enum name
	 * @return  the ID of the enum, or {@code null} if no enum value was found for the type with the name
	 * @see #findDynamic(Class, String)
	 */
	public Integer findAny(Class<? extends RedmineEnum> c, String name) {
		val ret = findDynamic(c, name);
		if (ret != null) { return ret; }
		
		for (val e : c.getEnumConstants()) {
			if (StringUtils.equalsIgnoreCase(e.getDisplayName(), name)) { return e.getId(); }
		}
		return null;
	}
}
