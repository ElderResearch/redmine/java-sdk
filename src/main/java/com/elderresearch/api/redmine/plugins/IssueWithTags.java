/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.api.redmine.Issue;
import com.elderresearch.api.redmine.Lists.ListWithPagination;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.util.RedminePaginator;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = true)
public class IssueWithTags extends Issue {
	@Getter
	private Set<String> tagList;
	
	public boolean addTag(String tag) {
		if (tagList == null) { tagList = new HashSet<>(); }
		return tagList.add(tag);
	}
	
	@JsonIgnore
	public void setTags(Collection<String> tagList) {
		if (tagList == null) {
			this.tagList = null;
		} else {
			if (tagList.isEmpty()) {
				// Otherwise, a blank set is considered null
				this.tagList = Collections.singleton(StringUtils.EMPTY);
			} else if (tagList instanceof Set) {
				this.tagList = Utilities.cast(tagList);
			} else {
				this.tagList = new HashSet<>(tagList);
			}
		}
	}

	@Data @EqualsAndHashCode(callSuper = true)
	public static class IssueWithTagsList extends ListWithPagination<IssueWithTags> {
		private List<IssueWithTags> issues;
		
		@Override public List<IssueWithTags> list() { return getIssues(); }
	}
	
	public static class IssueWithTagsPaginator extends RedminePaginator<IssueWithTags, IssueWithTagsList> {
		public IssueWithTagsPaginator(RedmineAPI api, WebParam... params) {
			super(IssueWithTagsList.class);
			setEndpoint(api.issues()::request).setParams(params);
		}
	}
}
