/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.contacts;

import static com.elderresearch.commons.lang.Utilities.cast;
import static com.elderresearch.commons.rest.client.RecursiveTarget.newTarget;

import java.util.List;

import com.elderresearch.api.redmine.RedmineAPI.IncludeParam;
import com.elderresearch.api.redmine.RedmineClient;
import com.elderresearch.api.redmine.plugins.contacts.Contact.ContactPaginator;
import com.elderresearch.api.redmine.plugins.contacts.Contact.ContactsList;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.RestEndpoint;
import com.elderresearch.commons.rest.client.RestEndpoint.RestEndpointWithIdInt;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

@Getter @Accessors(fluent = true)
public class CRMAPI {
	private final RedmineClient client;
	
	private ContactsAPI contacts;
	private ContactAPI  contact;
	
	public CRMAPI(RedmineClient client) {
		this.client = client;
	
		this.contacts       = new ContactsAPI();      
		this.contact        = new ContactAPI();       
	}
	
	@UtilityClass
    public class ContactsIncludeParam {
        public final String
            NOTES = "notes",
            CONTACTS = "contacts",
            DEALS = "deals",
            ISSUES = "issues";
        
        // The CRM plugin does CSV includes instead of multiple &include= query parameters
        public IncludeParam of(String... includes) { return IncludeParam.of(String.join(",", includes)); }
        public IncludeParam all() { return of(NOTES, CONTACTS, DEALS, ISSUES); }
    }

	public class ContactsAPI extends RestEndpoint {
		ContactsAPI() { super(client, newTarget("contacts")); }
		
		public List<Contact> getAll(WebParam... params) {
			return request(params).get(ContactsList.class).getContacts();
		}
		
		public ContactPost newPost(Contact c) {
			return cast(new ContactPost().setContact(c).to(CRMAPI.this));
		}
		
		public ContactPaginator paginator(WebParam... params) {
			return new ContactPaginator(CRMAPI.this, params);
		}
	}
	
	public class ContactAPI extends RestEndpointWithIdInt {
		ContactAPI() { super(client, contacts.target().child(idTemplate())); }
		
		public Contact get(int id, WebParam... params) {
			return LambdaUtils.apply(request(id, params).get(ContactPost.class), ContactPost::getContact);
		}
		
		public ContactPut newPut(Contact c) {
			return cast(new ContactPut(c).to(CRMAPI.this));
		}
	}
}
