/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.contacts;

import static com.elderresearch.commons.lang.LambdaUtils.apply;

import java.util.Date;
import java.util.List;

import com.elderresearch.api.redmine.CustomFieldValues;
import com.elderresearch.api.redmine.Issue;
import com.elderresearch.api.redmine.Lists.ListWithPagination;
import com.elderresearch.api.redmine.Upload;
import com.elderresearch.api.redmine.util.RedminePaginator;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.rest.client.Identifiable;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Contact extends CustomFieldValues {
	public enum ContactVisibility {
		PROJECT, PUBLIC, PRIVATE
	}
	
	private Integer id;
	private Integer projectId;
	
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	private ContactVisibility visibility;
	
	// Only one of these should be set
	private Boolean isCompany;
	private String company;
	
	private String firstName;
	private String lastName;
	private String middleName;
	
	private ContactAddress addressAttributes;
	private String website;
	private String skypeName;
	private Date birthday;
	private String jobTitle;
	private String background;
	private Integer authorId, assignedToId;
	
	private String phone;
	private List<ContactPhone> phones;
	
	private String email;
	private List<ContactEmail> emails;
	
	private Date createdOn;
	private Date updatedOn;
	
	private List<ContactNote> notes;
	private List<Issue> issues;
	
	private List<Upload> uploads;

	public Contact setAuthor(Identifiable i) {
		return setAuthorId(apply(i, Identifiable::getId));
	}
	
	public Contact setAddress(ContactAddressWrapper a) {
		return setAddressAttributes(a.getDetails());
	}
	
	public Contact setPhones(List<ContactPhone> phones) {
		this.phones = phones;
		LambdaUtils.accept(Utilities.first(phones), $ -> phone = $.getNumber());
		return this;
	}
	
	public Contact setEmails(List<ContactEmail> emails) {
		this.emails = emails;
		LambdaUtils.accept(Utilities.first(emails), $ -> email = $.getAddress());
		return this;
	}

	@Getter
	public static class ContactAddressWrapper {
		@JsonProperty("full_address")
		private ContactAddress details;
	}

	@Data @Accessors(chain = true)
	public static class ContactAddress {
		private String addressType, addressableType;
		private int addressableId;

		private Date createdAt;
		private Date updatedAt;
		private String fullAddress, street1, street2, city, region, country, countryCode, postcode;
	}
	
	@Data @Accessors(chain = true)
	public static class ContactPhone {
		private String number;
	}
	
	@Data @Accessors(chain = true)
	public static class ContactEmail {
		private String address;
	}
	
	@Data @Accessors(chain = true)
	public static class ContactNote {
		private int id;
		private String content;
		private Date createdOn;
		private Date updatedOn;
		private Integer authorId;
		
		public ContactNote setAuthor(Identifiable i) { return setAuthorId(apply(i, Identifiable::getId)); }
	}
	
	@Data @EqualsAndHashCode(callSuper = true)
	public static class ContactsList extends ListWithPagination<Contact> {
		private List<Contact> contacts;
		
		@Override public List<Contact> list() { return getContacts(); }
	}
	
	public static class ContactPaginator extends RedminePaginator<Contact, ContactsList> {
		public ContactPaginator(CRMAPI api, WebParam... params) {
			super(ContactsList.class);
			setEndpoint(api.contacts()::request).setParams(params);
		}
	}
}
