/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.contacts;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.util.PostBase;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class ContactPost extends PostBase<CRMAPI> {
	@Getter @Setter
	private Contact contact = new Contact();
	
	@Override
	protected Object getId() { return contact.getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.contacts().request(params).post(entity);
	}
	
	public Contact post() {
		return LambdaUtils.apply(persist(ContactPost.class), ContactPost::getContact);
	}
}
