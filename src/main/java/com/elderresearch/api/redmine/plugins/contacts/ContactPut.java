/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.contacts;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.util.PutBase;
import com.elderresearch.commons.rest.client.WebParam;

public class ContactPut extends PutBase<CRMAPI, Contact> {
	public ContactPut(Contact current) { super(Contact::new, current); }
	
	@Override
	protected Object getId() { return getCurrent().getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.contact().request(getCurrent().getId(), params).put(entity);
	}
}
