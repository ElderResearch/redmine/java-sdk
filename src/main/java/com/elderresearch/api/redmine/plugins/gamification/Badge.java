/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import java.util.Date;

import com.elderresearch.commons.rest.client.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class Badge extends Identifiable {
    private String description;
    private Integer authorId;
    private Integer priority;
    private Integer badgeGroupId;
    private Date createdOn, updatedOn;
    private Integer userId;
}
