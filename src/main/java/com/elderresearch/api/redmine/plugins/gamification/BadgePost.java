/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.util.PostBase;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class BadgePost extends PostBase<GamificationAPI> {
    @Getter @Setter
    private Badge badge = new Badge();

    @Override
    protected Object getId() { return badge.getName(); }

    @Override
    protected Response persist(Entity<?> entity, WebParam... params) {
        return api.badges().request(params).post(entity);
    }
    
	public Badge post() {
		return LambdaUtils.apply(persist(BadgePost.class), BadgePost::getBadge);
	}
}
