/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum BadgeStatus {
    GRANTED("granted"),
    REJECTED("rejected"),
    NOMINATED("nominated");

    @Getter(onMethod_ = @JsonValue)
    private String status;
}
