/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import static com.elderresearch.commons.lang.Utilities.cast;
import static com.elderresearch.commons.rest.client.RecursiveTarget.newTarget;

import java.util.List;

import com.elderresearch.api.redmine.Lists;
import com.elderresearch.api.redmine.RedmineClient;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.RestEndpoint;
import com.elderresearch.commons.rest.client.RestEndpoint.RestEndpointWithIdInt;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter @Accessors(fluent = true)
public class GamificationAPI {
	private final RedmineClient client;
	
	private BadgesAPI     badges;
	private BadgeAPI      badge;
	private NominationAPI nominations;
	
	public GamificationAPI(RedmineClient client) {
		this.client = client;
		
		this.badges      = new BadgesAPI();    
		this.badge       = new BadgeAPI();     
		this.nominations = new NominationAPI();
	}

    public class BadgesAPI extends RestEndpoint {
    	BadgesAPI() { super(client, newTarget("badges")); }

        public List<Badge> getAll(WebParam... params) {
            return request(params).get(BadgesList.class).getBadges();
        }
        
        public BadgePost newPost(Badge b) {
        	return cast(new BadgePost().setBadge(b).to(GamificationAPI.this));
        }
    }

    public class BadgeAPI extends RestEndpointWithIdInt {
    	BadgeAPI() { super(client, badges.target().child(idTemplate())); }

        public Badge get(int id, WebParam... params) {
            return LambdaUtils.apply(request(id, params).get(BadgePost.class), BadgePost::getBadge);
        }
    }

    public class NominationAPI extends RestEndpoint {
    	NominationAPI() { super(client, newTarget("nominations")); }

        public NominationPost newPost(Nomination n) {
        	return cast(new NominationPost().setNomination(n).to(GamificationAPI.this));
        }
    }

    @Data @Accessors(fluent = false)
    @EqualsAndHashCode(callSuper = true)
    public static class BadgesList extends Lists.ListWithPagination<Badge> {
        private List<Badge> badges;

        @Override public List<Badge> list() { return getBadges(); }
    }
}
