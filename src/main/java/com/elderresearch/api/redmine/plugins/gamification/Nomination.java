/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import com.elderresearch.commons.rest.client.Identifiable;
import com.webcohesion.enunciate.metadata.ReadOnly;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Nomination {
    private Integer id;

    private Integer nomineeId;
    private Integer authorId;
    private Integer badgeId;
    private Integer projectId;
    private String justification;

    private boolean forceApproval;

    @ReadOnly
    private BadgeStatus status;

    public void setNominee(Identifiable nominee) { nomineeId = nominee.getId(); }
    public void setAuthor(Identifiable author) { authorId = author.getId(); }
    public void setBadge(Identifiable badge) { badgeId = badge.getId(); }
}
