/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.gamification;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.util.PostBase;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class NominationPost extends PostBase<GamificationAPI> {
    @Getter @Setter
    private Nomination nomination = new Nomination();

    @Override
    protected Object getId() {
        return nomination.getId();
    }

    @Override
    protected Response persist(Entity<?> entity, WebParam... params) {
        return api.nominations().request(params).post(entity);
    }
    
	public Nomination post() {
		return LambdaUtils.apply(persist(NominationPost.class), NominationPost::getNomination);
	}
}

