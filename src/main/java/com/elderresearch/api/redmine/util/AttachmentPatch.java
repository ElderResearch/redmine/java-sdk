/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.Upload;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class AttachmentPatch extends PutBase<RedmineAPI, Upload> {
	public AttachmentPatch(Upload current) { super(Upload::new, current); }
	 
	@Override @JsonProperty("attachment")
	public Upload getPayload() { return super.getPayload(); }
	
	@Override
	protected Object getId() { return getCurrent().getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.attachments().request(getCurrent().getId(), params).method(HttpMethod.PATCH, entity);
	}
}
