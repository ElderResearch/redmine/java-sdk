/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import java.util.List;

import lombok.Data;

@Data
public class ErrorList {
	private List<String> errors;
}
