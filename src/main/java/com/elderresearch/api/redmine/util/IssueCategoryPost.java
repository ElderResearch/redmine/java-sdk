/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.IssueCategory;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class IssueCategoryPost extends PostBase<RedmineAPI> {
	@Setter
	private transient int projectId;
	
	@Getter @Setter
	private IssueCategory issueCategory = new IssueCategory();
	
	@Override
	protected Object getId() { return issueCategory.getName(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.issueCategories().request(projectId, params).post(entity);
	}
	
	public IssueCategory post() {
		return LambdaUtils.apply(persist(IssueCategoryPost.class), IssueCategoryPost::getIssueCategory);
	}
}
