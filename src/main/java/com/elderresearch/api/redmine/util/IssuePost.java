/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.Issue;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.RedmineClient;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.databind.type.TypeFactory;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Accessors(chain = true)
public class IssuePost<I extends Issue> extends PostBase<RedmineAPI> {
	private static final TypeFactory typeFactory = RedmineClient.objectMapper().getTypeFactory();
	
	@Getter @Setter
	private I issue;
	
	@Override
	protected Object getId() { return issue.getSubject(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.issues().request(params).post(entity);
	}
	
	public I post() {
		return post(log::warn);
	}

	@SuppressWarnings("unchecked")
	public I post(Consumer<String> errorHandler) {
		// Only Jackson is able to correctly handle the specific Issue subclass on deserialization.
		// Jersey's GenericType loses I due to erasure.
		val type    = typeFactory.constructParametricType(getClass(), issue.getClass());
		val reader  = RedmineClient.objectMapper().readerFor(type);
        val wrapper = (IssuePost<I>) persist(resp -> {
			try {
				// Jackson auto-closes input stream unless JsonParser.Feature.AUTO_CLOSE_SOURCE is disabled
				return reader.readValue((InputStream) resp.getEntity());
			} catch (IOException | ClassCastException e) {
				throw new ProcessingException(e); 
			}
		}, resp -> {
			resp.readEntity(ErrorList.class).getErrors().forEach(errorHandler);
		});
        return LambdaUtils.apply(wrapper, IssuePost::getIssue);
	}
}
