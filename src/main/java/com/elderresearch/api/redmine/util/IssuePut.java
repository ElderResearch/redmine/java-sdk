/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.Issue;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Supplier;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public abstract class IssuePut<I extends Issue> extends PutBase<RedmineAPI, I> {
	protected IssuePut(Supplier<I> issueFactory, I current) { super(issueFactory, current); }
	
	@Override @JsonProperty("issue")
	public I getPayload() { return super.getPayload(); }
	
	@Override
	protected Object getId() { return getCurrent().getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.issue().request(getCurrent().getId(), params).put(entity);
	}

	public static class IssuePutBase extends IssuePut<Issue> {
		// Concrete class for Jackson/reflection
		public IssuePutBase(Issue current) { super(Issue::new, current); }
	}
}
