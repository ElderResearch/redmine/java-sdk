/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.IssueRelation;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class IssueRelationPost extends PostBase<RedmineAPI> {
	@Setter
	private transient int issueId;
	
	@Getter @Setter
	private IssueRelation relation = new IssueRelation();
	
	@Override
	protected Object getId() { return issueId; }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.issueRelations().request(issueId, params).post(entity);
	}
}
