/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import com.elderresearch.api.redmine.RedmineAPI.KeyHeader;
import com.elderresearch.commons.rest.client.JSONPersistable;

public abstract class PersistBase<API> extends JSONPersistable {
	protected API api;
	
	@SuppressWarnings("hiding")
	public PersistBase<API> to(API api) {
		this.api = api;
		return this;
	}
	
	public JSONPersistable asUser(String apiKey) {
		return setRequestParams(KeyHeader.of(apiKey));
	}
}
