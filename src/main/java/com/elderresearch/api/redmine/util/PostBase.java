/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

public abstract class PostBase<API> extends PersistBase<API> {
	// No additional methods - creating for cleaner hierarchy and ease of extension in the future
}
