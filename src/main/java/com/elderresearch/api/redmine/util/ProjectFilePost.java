/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.Upload;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class ProjectFilePost extends PostBase<RedmineAPI> {
	@Setter
	private transient int projectId;
	
	@Getter @Setter
	private Upload file = new Upload();
	
	@Override
	protected Object getId() { return file.getToken(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.projectFiles().request(projectId, params).post(entity);
	}
}
