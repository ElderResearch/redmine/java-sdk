/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.Project;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class ProjectPost extends PostBase<RedmineAPI> {
	@Getter @Setter
	private Project project = new Project();
	
	@Override
	protected Object getId() { return project.getIdentifier(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.projects().request(params).post(entity);
	}
	
	public Project post() {
		return LambdaUtils.apply(persist(ProjectPost.class), ProjectPost::getProject);
	}
}
