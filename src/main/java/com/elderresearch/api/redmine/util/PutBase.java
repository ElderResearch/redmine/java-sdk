/* ©2020-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.api.redmine.HasCustomValues;
import com.elderresearch.api.redmine.enums.CustomFieldEnum;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.lang.Utilities;
import com.elderresearch.commons.rest.client.JSONPersistable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Iterables;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.experimental.Accessors;

/**
 * <p>Save changes to a Redmine object. This class has two "modes" of comparison. By default, a new, blank object, or
 * "payload", is created and callers can set attributes of that object via {@link #withChanges(Consumer)}. If any fields
 * are set, {@link #shouldPersist()} returns {@code true} and {@link #persist()} will save only the fields that were
 * set. This example changes the subject of an existing issue:</p>
 * <pre>
 * new IssuePutBase(existing).withChanges($ &rarr; $.setSubject("New subject")).persist();
 * </pre>
 * <p>Alternatively, this can be used to merge or "diff" two existing objects. If {@link #setPayload(Object)} is called
 * with object {@code o}, fields can be merged with {@link #merge(Merger, Accessor...)}, where either they are set to
 * {@code null} (meaning the current value is kept), the value from {@code o} is kept (meaning it will be changed),
 * or some combination of the current and new value is saved. This example changes the assignee only if an assignee
 * isn't already set:</p>
 * <pre>
 * val i = new Issue().setAssignedToId(2);
 * val ip = new IssuePutBase(existing).mergeWith(i);
 * ip.merge(Merger.keepExisting(), Accessor.with(Issue::getAssignedToId, Issue::setAssignedToId));
 * if (ip.shouldPersist()) { ip.persist(); }
 * </pre>
 * <P>Subclasses will need to do four things:<ul>
 * <li>Specify the current object and how to create a "blank" payload object to the protected constructor.
 * Usually this is just the constructor of the concrete class (for e.g. {@code User::new})</li>
 * <li>Specify how to get the ID of the current object (from {@link JSONPersistable}). Usually this is just
 * {@code getCurrent().getId()}.</li>
 * <li>Actually perform the {@code PUT} against a Redmine API (from {@link JSONPersistable}).</li>
 * <li>Override {@link #getPayload()} and annotate with a {@link JsonProperty} annotation to change the property
 * name to match what the API expects.</li></ul>
 * 
 * @author <a href="dimeo@elderresearch.com">John Dimeo</a>
 * @since Jun 1, 2020
 * @param <T> the concrete subclass of the object being changed by this {@code PUT} operation
 */
@Accessors(chain = true)
public abstract class PutBase<API, T> extends PersistBase<API> {
	private transient final Supplier<T> payloadFactory;
	
	@Getter @JsonIgnore
	private final transient T current;
	
	@Getter @Setter
	private T payload;
	
	protected PutBase(Supplier<T> payloadFactory, T current) {
		this.payloadFactory = payloadFactory;
		this.current = current;
		this.payload = payloadFactory.get();
	}
	
	/**
	 * Apply the following changes to the payload, providing chained calls.
	 * @param callback the callback to apply to the payload
	 * @return this for method chaining
	 */
	public PutBase<API, T> withChanges(Consumer<T> callback) {
		callback.accept(getPayload());
		return this;
	}
	
	/**
	 * Tests whether or not the payload was actually changed (meaning it is not equal to a
	 * blank payload and has at least one non-{@code null} field).
	 * @return if there are updates/changes to persist to the payload
	 */
	public boolean shouldPersist() {
		return !Objects.equals(payloadFactory.get(), getPayload());
	}
	
	public interface Accessor<I, T> {
		void set(I i, T t);
		T getExisting(I i);
		default T getNew(I i) { return getExisting(i); }
		
		static <I, T> Accessor<I, T> with(Function<I, T> getter, BiConsumer<I, T> setter) {
			return new Accessor<I, T>() {
				@Override public T getExisting(I i) { return getter.apply(i); }
				@Override public void set(I i, T t) { setter.accept(i, t); }
			};
		}
		
		static <I, T> Accessor<I, T> with(Function<I, T> getExisting, Function<I, T> getNew, BiConsumer<I, T> setter) {
			return new Accessor<I, T>() {
				@Override public T getExisting(I i) { return getExisting.apply(i); }
				@Override public T getNew(I i) { return getNew.apply(i); }
				@Override public void set(I i, T t) { setter.accept(i, t); }
			};
		}
		
		static <I extends HasCustomValues, T> Accessor<I, T> forField(CustomFieldEnum e) {
			return new Accessor<I, T>() {
				@Override public T getExisting(I i) { return Utilities.cast(e.get(i)); }
				@Override public void set(I i, T t) { LambdaUtils.accept(t, $ -> e.set(i, $)); }
			};
		}
		
		static <I extends HasCustomValues, T> Accessor<I, T> forField(CustomFieldEnum e, I newValues) {
			return new Accessor<I, T>() {
				@Override public T getExisting(I i) { return Utilities.cast(e.get(i)); }
				@Override public T getNew(I i) { return Utilities.cast(e.get(newValues)); }
				@Override public void set(I i, T t) { LambdaUtils.accept(t, $ -> e.set(i, $)); }
			};
		}
	}
	
	@FunctionalInterface
	public interface Merger<T> extends BinaryOperator<T> {
		default boolean isUndefined(Object o) {
			return o == null || StringUtils.isBlank(o.toString()) ||
					(o instanceof Iterable<?> && Iterables.isEmpty((Iterable<?>) o));
		}
		
		static <T> Merger<T> keepExisting() {
			return new Merger<T>() {
				// If the current value is defined, RETURN null so there is no change; otherwise, set the new value
				@Override public T apply(T v1, T v2) { return !isUndefined(v1)? null : v2; }
			};
		}
		
		static <T> Merger<T> useNew() {
			return new Merger<T>() {
				// If the new value is defined, return that value so it is changed; otherwise, return null so it isn't				
				@Override public T apply(T v1, T v2) { return !isUndefined(v2)? v2 : null; }
			};
		}
		
		static Merger<String> concat(String delim) {
			return new Merger<String>() {
				@Override
				public String apply(String v1, String v2) {
					if (isUndefined(v1)) { return v2; }
					if (isUndefined(v2)) { return null; }
					return v1 + delim + v2;
				}
			};
		}
		
		static Merger<String> concatUnique(String delim) {
			return new Merger<String>() {
				@Override
				public String apply(String v1, String v2) {
					if (isUndefined(v1)) { return v2; }
					if (isUndefined(v2) || v1.contains(v2)) { return null; }
					return v1 + delim + v2;
				}
			};
		}
	}
	
	/**
	 * Merge the changes to the field using the current payload's value and the new payload's value as inputs.
	 * If the current value should <em>not</em> be changed, the merger should return {@code null}. Otherwise, it 
	 * should return the second value (from the new payload) or some combination of the two. 
	 * @param <U> the type of the field
	 * @param merger the merge strategy
	 * @param fields one or more fields to merge with the strategy
	 * @return this for method chaining
	 * @see Merger Merger's factories for common merge implementations.
	 */
	@SafeVarargs
	public final <U> PutBase<API, T> merge(Merger<U> merger, Accessor<T, U>... fields) {
		for (val field : fields) {
			field.set(getPayload(), merger.apply(field.getExisting(getCurrent()), field.getNew(getPayload())));
		}
		return this;
	}
}
