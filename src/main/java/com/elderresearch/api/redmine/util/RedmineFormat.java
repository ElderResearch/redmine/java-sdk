/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.jooq.lambda.Unchecked;

import com.elderresearch.commons.lang.LambdaUtils;
import com.google.common.collect.ImmutableSet;

import lombok.experimental.UtilityClass;

/**
 * Formats data into a representation understood by Redmine.
 * 

 * @since Nov 8, 2017
 */
@UtilityClass
public class RedmineFormat {
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final FastDateFormat FDF = FastDateFormat.getInstance(DATE_FORMAT);
	private static final Set<String> BOOL_TRUE = ImmutableSet.of("yes", "true", "y", "1");
	
	public String dateToString(Object d) {
		return LambdaUtils.apply(d, FDF::format);
	}
	
	public Object stringToDate(String s) {
		return LambdaUtils.apply(StringUtils.stripToNull(s), Unchecked.function(FDF::parse));
	}
	
	public String booleanToString(Object b) {
		if (b instanceof Boolean) { return booleanToString((boolean) b); }
		if (b instanceof Number)  { return booleanToString(((Number) b).longValue() > 0L); }
		return booleanToString(BOOL_TRUE.contains(Objects.toString(b).toLowerCase()));
	}
	
	private String booleanToString(boolean b) { return b? "1" : "0"; }
	
	public Object stringToBoolean(String s) {
		return NumberUtils.toInt(s) > 0;
	}
	
	/**
	 * Escapes any hash ({@code #}) characters in the string to prevent them from being rendered as issue links.
	 * @param s the string to escape
	 * @return the escaped strings where hash characters aren't rendered as issue links
	 */
	public String escapeHash(String s) {
		return StringUtils.replace(s, "#", "!#");
	}
}
