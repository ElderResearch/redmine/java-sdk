/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import jakarta.ws.rs.client.Invocation;

import org.apache.commons.lang3.ArrayUtils;

import com.elderresearch.api.redmine.Issue;
import com.elderresearch.api.redmine.Lists.HasListOf;
import com.elderresearch.api.redmine.Lists.IssueList;
import com.elderresearch.api.redmine.Lists.SearchResultList;
import com.elderresearch.api.redmine.Lists.UserList;
import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.RedmineAPI.PageParams;
import com.elderresearch.api.redmine.SearchResult;
import com.elderresearch.api.redmine.User;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;
import lombok.experimental.Accessors;

@RequiredArgsConstructor
@Accessors(chain = true)
public class RedminePaginator<T, L extends HasListOf<T>> {
	private static final int PAGE = 50;

	private final Class<L> listType;
	
	@Setter
	private Function<WebParam[], Invocation.Builder> endpoint;
	
	private WebParam[] params = {};
	
	public RedminePaginator<T, L> setParams(WebParam... params) {
		this.params = params;
		return this;
	}
	
	public void forEach(Consumer<? super T> callback) {
		boolean more = true;
		for (int i = 0; more; i += PAGE) {
			val list = page(i, PAGE);
			list.forEach(callback);
			more = list.size() == PAGE;
		}	
	}
	
	public List<T> page(long offset, long limit) {
		val group = ArrayUtils.addAll(params, PageParams.of(offset, limit));
		return endpoint.apply(group).get(listType).list();
	}
	
	public static class IssuePaginator extends RedminePaginator<Issue, IssueList> {
		public IssuePaginator(RedmineAPI api, WebParam... params) {
			super(IssueList.class);
			setEndpoint(api.issues()::request).setParams(params);
		}
	}
	
	public static class UserPaginator extends RedminePaginator<User, UserList> {
		public UserPaginator(RedmineAPI api, WebParam... params) {
			super(UserList.class);
			setEndpoint(api.users()::request).setParams(params);
		}
	}
	
	public static class SearchPaginator extends RedminePaginator<SearchResult, SearchResultList> {
		public SearchPaginator(RedmineAPI api, WebParam... params) {
			super(SearchResultList.class);
			setEndpoint(api.search()::request).setParams(params);
		}
	}
}
