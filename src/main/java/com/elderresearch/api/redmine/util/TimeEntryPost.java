/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.TimeEntry;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class TimeEntryPost extends PostBase<RedmineAPI> {
	@Getter @Setter
	private TimeEntry timeEntry = new TimeEntry();
	
	@Override
	protected Object getId() { return timeEntry.getIssueId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.timeEntries().request(params).post(entity); 
	}
	
	public TimeEntry post() {
		return LambdaUtils.apply(persist(TimeEntryPost.class), TimeEntryPost::getTimeEntry);
	}
}
