/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.TimeEntry;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeEntryPut extends PutBase<RedmineAPI, TimeEntry> {
	public TimeEntryPut(TimeEntry current) { super(TimeEntry::new, current); }
	
	@Override @JsonProperty("time_entry")
	public TimeEntry getPayload() { return super.getPayload(); }
	
	@Override
	protected Object getId() { return getCurrent().getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.timeEntry().request(getCurrent().getId(), params).put(entity);
	}
}
