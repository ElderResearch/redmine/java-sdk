/* ©2018-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class UserMembershipPost extends PostBase<RedmineAPI> {
	@Setter
	private transient int projectId;

	@Getter @Setter
	private Membership membership;
	
	@Override
	protected Object getId() { return membership.toString(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.projectMembership().request(projectId, params).post(entity);
	}
	
	@Getter @ToString
	@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
	public static class Membership {
		private int userId;
		private int[] roleIds;
	}
}
