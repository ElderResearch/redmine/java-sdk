/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.User;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class UserPost extends PostBase<RedmineAPI> {
	@Getter @Setter
	private User user = new User();

    public UserPost() {
        super();
    }

    public UserPost(User user) {
        super();
        this.user = user;
    }

    @Override
	protected Object getId() { return user.getLogin(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.users().request(params).post(entity);
	}
	
	public User post() {
		return LambdaUtils.apply(persist(UserPost.class), UserPost::getUser);
	}
}
