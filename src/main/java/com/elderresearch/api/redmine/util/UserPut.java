/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.User;
import com.elderresearch.commons.rest.client.WebParam;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class UserPut extends PutBase<RedmineAPI, User> {
	public UserPut(User current) { super(User::new, current); }
	
	@Override @JsonProperty("user")
	public User getPayload() { return super.getPayload(); }
	
	@Override
	protected Object getId() { return getCurrent().getId(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.user().request(getCurrent().getId(), params).put(entity);
	}
}
