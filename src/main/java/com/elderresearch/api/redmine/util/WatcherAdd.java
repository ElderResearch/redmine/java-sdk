/* ©2017-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@RequiredArgsConstructor
public class WatcherAdd extends PostBase<RedmineAPI> {
	@Getter(AccessLevel.PROTECTED)
	private final transient int issueId;
	
	@Getter @Setter
	private int userId;
	
	@Override
	protected Object getId() { return userId; }

	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.watchers().forIssue(issueId).request(params).post(entity);
	}
}
