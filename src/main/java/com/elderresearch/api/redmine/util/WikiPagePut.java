/* ©2019-2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.util;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.WikiPage;
import com.elderresearch.commons.rest.client.WebParam;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

// Wiki pages are both created and updated via PUT and also don't use the "null to not change" paradigm (the text is
// always required) so this doesn't inherit from PutBase<>
@Accessors(chain = true)
public class WikiPagePut extends PersistBase<RedmineAPI> {
	@Setter
	private transient int projectId;
	
	@Getter @Setter
	private WikiPage wikiPage = new WikiPage();
	
	@Override
	protected Object getId() { return wikiPage.getTitle(); }
	
	@Override
	protected Response persist(Entity<?> entity, WebParam... params) {
		return api.wikiPages().request(projectId, wikiPage.getTitle(), params).put(entity);
	}
}

