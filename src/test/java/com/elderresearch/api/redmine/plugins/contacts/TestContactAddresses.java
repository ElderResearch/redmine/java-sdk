/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.api.redmine.plugins.contacts;

import com.elderresearch.api.redmine.RedmineClient;
import com.elderresearch.api.redmine.RedmineConfig;
import com.elderresearch.api.redmine.plugins.contacts.Contact.ContactAddress;

import lombok.val;

public class TestContactAddresses {
	public static void main(String[] args) {
		val config = new RedmineConfig();
		config.setUrl("http://localhost:3000/");
		config.setLogRequests(true);
		config.setKey(System.getenv("REDMINE_KEY"));
		
		val crm = new CRMAPI(new RedmineClient(config));
		
		val fresh = crm.contact().get(1);
		
		System.out.println(fresh);
		
		crm.contact().newPut(fresh).withChanges(c -> {
			c.setAddressAttributes(new ContactAddress().setCountryCode("DE"));
		}).persist();
		
		crm.contacts().newPost(fresh.setProjectId(1)).post();
	}
}
